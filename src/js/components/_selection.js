export default () => {

    document.addEventListener('click', Selection);

    function Selection(event) {
        const selections = document.querySelectorAll('[data-selection]');
        if (selections.length > 0) {

            if (event.target.closest('[data-selection-chosen]')) {
                removeChosen();
            }

            else if (event.target.closest('[data-selection-toggle]')) {
                let selectionContainer = event.target.closest('[data-selection]');
                if (selectionContainer.classList.contains('open')) {
                    selectionContainer.classList.remove('open');
                }
                else {
                    SelectionClose();
                    selectionContainer.classList.add('open');
                    console.log(       isMultiple(event));
                }
            }

            else if (event.target.closest('[data-selection-item]')) {
                if (isMultiple(event)) {
                    SelectionToggle();
                    SelectionChoice(event);
                }
                else {
                    const selectionContainer = event.target.closest('[data-selection]');
                    const selectionItems = selectionContainer.querySelectorAll('[data-selection-item]');
                    let selectionValue = event.target.closest('[data-selection-item]').dataset.selectionItem;

                    selectionItems.forEach(elem => {
                        elem.classList.remove('selected');
                    });
                    event.target.closest('[data-selection-item]').classList.toggle('selected');
                    selectionContainer.querySelector('[data-selection-list]').innerHTML = '<span class="selection__elem" data-selection-chosen="' + selectionValue + '">' + selectionValue + '</span>';
                    isSelected(selectionContainer);
                    SelectionClose();
                }
            }

            else {
                SelectionClose();
            }

            function SelectionClose() {
                selections.forEach(el => {
                    el.classList.remove('open');
                });
            }

            function SelectionToggle() {
                event.target.closest('[data-selection-item]').classList.toggle('selected');

            }

            function SelectionChoice() {
                let selectionContainer = event.target.closest('[data-selection]');
                let selectionList = selectionContainer.querySelector('[data-selection-list]');
                let selectionValue = event.target.closest('[data-selection-item]').dataset.selectionItem;
                let selectionDiv = document.createElement('span');

                if(event.target.closest('[data-selection-item]').classList.contains('selected')) {
                    selectionDiv.classList.add('selection__elem');
                    selectionDiv.textContent = selectionValue;
                    selectionDiv.setAttribute('data-selection-chosen', selectionValue);
                    selectionList.appendChild(selectionDiv);
                }
                else {
                    selectionList.querySelector(`[data-selection-chosen="${selectionValue}"]`).remove();
                }

                isSelected(selectionContainer);
            }

            function removeChosen(){
                let selectionContainer = event.target.closest('[data-selection]');
                let selectionValue = event.target.closest('[data-selection-chosen]').dataset.selectionChosen;
                selectionContainer.querySelector(`[data-selection-chosen="${selectionValue}"]`).remove();
                selectionContainer.querySelector(`[data-selection-item="${selectionValue}"]`).classList.remove('selected');

                isSelected(selectionContainer);
            }

            function isMultiple(event) {
                return event.target.closest('[data-selection]').classList.contains('selection--multiple');
            }

            function isSelected(selectionContainer) {
                if(selectionContainer.querySelectorAll('[data-selection-chosen]').length > 0) {
                    selectionContainer.classList.add('selection--choose');
                }
                else  {
                    selectionContainer.classList.remove('selection--choose');
                }
            }
        }
    }
};
